# HaP Snake Game
A toy arduino snake game built with a LED Matrix as display and a joystick as input device.

Demo video: https://www.youtube.com/watch?v=Y5qZ6YtVEtI

Learn more about this project: https://archive.hackersatporto.com/1920/workshops/snake/
## TODO
* Use interrupts

